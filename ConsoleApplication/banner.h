﻿#pragma once
#include <vector>
#include <string>

using namespace std;

class Banner {
public:
    Banner() = delete;
    Banner(const int& in_price, const int& in_camp_id, vector<string>& in_countries, const int& in_id) noexcept :
        m_price(in_price), m_camp_id(in_camp_id), m_countries(in_countries), m_id(in_id)
    {};

    Banner(const int& in_price, const int& in_camp_id, initializer_list <string> in_countries, const int& in_id) noexcept :
        m_price(in_price), m_camp_id(in_camp_id), m_countries(in_countries), m_id(in_id)
    {};

    Banner(const Banner& other) 
    {
        m_price = other.m_price;
        m_camp_id = other.m_camp_id;
        m_countries = other.m_countries;
        m_id = other.m_id;
    };
    Banner(Banner&& other) noexcept
    {
        m_price = std::move(other.m_price);
        m_camp_id = std::move(other.m_camp_id);
        m_countries = std::move(other.m_countries);
        m_id = std::move(other.m_id);
    };
    Banner& operator=(const Banner& other) 
    {
        m_price = other.m_price;
        m_camp_id = other.m_camp_id;
        m_countries = other.m_countries;
        m_id = other.m_id;
        return *this;
    };
    Banner& operator=(Banner&& other) noexcept
    {
        m_price = std::move(other.m_price);
        m_camp_id = std::move(other.m_camp_id);
        m_countries = std::move(other.m_countries);
        m_id = std::move(other.m_id);
        return *this;
    };

    ~Banner() = default;

    bool operator==(Banner& other)
    {

        if (m_price != other.m_price) return false;
        else if (m_camp_id != other.m_camp_id) return false;
        else if (!equal(m_countries.begin(), m_countries.end(), other.m_countries.begin())) return false;
        else if (m_id != other.m_id) return false;
        else return true;
    };

    const int& getCampaign() const { return m_camp_id; };
    const int& getPrice() const { return m_price; };
    const vector<string>& getCountry() const { return m_countries; };
    const int& getID() const { return m_id; };
private:
    int m_price;
    int m_camp_id;
    int m_id;
    vector<string> m_countries;

};


vector<Banner> getByCountryAndCampaign(const string& country, const int& campaign, vector<Banner>& input);

vector<Banner> getHighestPriceVector(const size_t& N, vector<Banner>& input);

int getTotalPrice(const vector<Banner>& input);

vector<Banner> getHighest(const int count, const string& country, vector<Banner>& input);

