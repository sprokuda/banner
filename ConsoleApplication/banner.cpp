﻿#include <vector>
#include<string>
#include <iostream>
#include <algorithm>
#include <set>
#include <map>

#include "banner.h"
#include "test.h"

using namespace std;


vector<Banner> getByCountryAndCampaign(const string& country, const int& campaign, vector<Banner>& input)
{
    auto equal_to_country = [&country](const Banner& b1)
    {
        vector<string> v1 = b1.getCountry();
        return find_if(
            v1.begin(), v1.end(), [&country](const string& s) { return s.compare(country) == 0 || s.empty(); }
        ) != v1.end() || v1.empty() || country.empty();
    };

    auto border = partition(input.begin(), input.end(), equal_to_country);
    vector<Banner> the_same_country(input.begin(), border);

    auto equal_to_campaign = [&campaign](const Banner& s)
    {
        return s.getCampaign() == campaign;
    };

    border = partition(the_same_country.begin(), the_same_country.end(), equal_to_campaign);

    vector<Banner> the_same_campaign(the_same_country.begin(), border);

    //    auto comp_price = [](const Banner& b1, const Banner& b2) { return b1.getPrice() > b2.getPrice(); };

    //    sort(the_same_campaign.begin(), the_same_campaign.end(), comp_price);

    return the_same_campaign;
}

vector<Banner> getHighestPriceVector(const size_t& N, vector<Banner>& input)
{
    auto comp_price = [](const Banner& b1, const Banner& b2) { return b1.getPrice() > b2.getPrice(); };

    sort(input.begin(), input.end(), comp_price);

    if (N == 0)
    {
        vector<Banner>  result;
        return result;
    }
    else if (N < input.size())
    {
        auto last = input.begin() + N-1;
        auto bounds = equal_range(input.begin(), input.end(), *last, comp_price);

        if (last == bounds.first)
        {
            vector<Banner> result(input.begin(), bounds.first);
            vector<Banner> smallest_price(bounds.first, bounds.second);

            if (smallest_price.size() > 1)
            {
                int n = rand() % smallest_price.size();
                result.push_back(smallest_price[n]);
            }
            else
            {
                result.push_back(*bounds.first);
            }

            return result;
        }
        else
        {
            vector<Banner> result(input.begin(), last);
            vector<Banner> smallest_price(last, bounds.second);

            if (smallest_price.size() > 1)
            {
                int n = rand() % smallest_price.size();
                result.push_back(smallest_price[n]);
            }
            else
            {
                result.push_back(*bounds.first);
            }

            return result;
        }
    }
    else return input;

}

int getTotalPrice(const vector<Banner>& input)
{
    int total_price = 0;
    for (auto& item : input)
    {
        total_price = total_price + item.getPrice();
    }
    return total_price;
}

vector<Banner> getHighest(const int count, const string& country, vector<Banner>& input)
{
    set<int> campaigns;
    for (auto& item : input)
    {
        campaigns.insert(item.getCampaign());
    }
    map<int, vector<Banner>> m;

    for (auto& item : campaigns)
    {
        auto camp_and_country = getByCountryAndCampaign(country, item, input);
        auto output = getHighestPriceVector(count, camp_and_country);
        int price = getTotalPrice(output);
        m.emplace(price, output);
    }

    return m.rbegin()->second;
}


int main()
{
    try
    {
        auto vec = generateTestVector(22);
        auto output = getHighest(4u, FR, vec);
    }
    catch (runtime_error& e)
    {
        cout << e.what() << endl;
    }
    catch (...)
    {
        cout << "some other expection happened" << endl;
    }

    TestAll();

}

