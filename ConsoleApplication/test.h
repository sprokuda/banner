﻿#pragma once
#include <string>
#include <functional>
#include <exception>
#include <algorithm>
#include <iostream>
#include <sstream> 

#include "banner.h"

using namespace std;


string DE = "DE";
string RU = "RU";
string FR = "FR";
string IT = "IT";

vector<string> c0({ });
vector<string> c1({ DE, RU, FR });
vector<string> c2({ IT, DE, FR });
vector<string> c3({ IT, RU });


vector<Banner> generateTestVector(const int& in_size)
{

    vector<Banner> result;

    for (int i = 0; i < in_size; i++)
    {
        int id = i;
        int camp_id = rand() % 3;
        int price = rand() % 3 + 1;
        int n_country = rand() % 5;
        vector<string> countries;
        switch (n_country)
        {
        case 0:
            countries = c0;
            break;
        case 1:
            countries = c1;
            break;
        case 2:
            countries = c2;
            break;
        case 3:
            countries = c3;
            break;
        case 4:
            //            countries = c2;
            countries.push_back("");
            break;
        default:
            countries = { "" };
            break;
        }
        result.push_back( move(Banner(price, camp_id, countries, id)) );
    }
    return result;
}

class TestRunner {
public:
	void RunTest(function<void (void)> func, const string& test_name) {
			try { 
			func();
			cerr << test_name << " OK" << endl;
		}
		catch (runtime_error& e) {
			++fail_count; 
			cerr << test_name << " fail: " << e.what() << endl;
		}
	}
	~TestRunner() { 
		if (fail_count > 0) { 
			cerr << fail_count << " unit tests failed. Terminate" << endl;
			exit(1); 
		}
	}
private:
	int fail_count = 0;
};


void AssertVectorEqual(vector<Banner>& t, vector<Banner>& u, const string& hint) 
{
	auto comp_id = [](const Banner& b1, const Banner& b2) { return b1.getID() < b2.getID(); };

	sort(t.begin(), t.end(), comp_id);
	sort(u.begin(), u.end(), comp_id);
    if(t.size() != u.size())
        throw runtime_error("Vector's sizes mismatch\n");

	if (!equal(t.begin(), t.end(), u.begin())) {
		ostringstream os;
		os << "Assertion failed: " << "Hint: " << hint;
		throw runtime_error(os.str());
	}
}

void AssertEqual(const int& t, const int& u, const string& hint)
{

    if (t != u)
    {
        ostringstream os;
        os << "Assertion failed: " << t <<"!=" <<u << "Hint: " << hint;
        throw runtime_error(os.str());
    }
}

void Test_basic()
{
	vector<Banner> test = { Banner(2,0, {FR,RU},1000) , Banner(3, 0, { IT,RU }, 1001), Banner(3, 0, { FR,IT }, 1002) };
	vector<Banner> result = { Banner(3, 0, { IT,RU }, 1001), Banner(3, 0, { FR,IT }, 1002) };

	auto camp_and_country = getByCountryAndCampaign("IT", 0, test);
	auto output = getHighestPriceVector(5u, camp_and_country);

    AssertVectorEqual(output, result,"");
}

void Test_empty_country()
{
    vector<Banner> test = { Banner(2,0, {FR,RU},1000) , Banner(3, 0, { IT,RU }, 1001), Banner(3, 0, { FR,IT }, 1002) };
    vector<Banner> result = test;

    auto camp_and_country = getByCountryAndCampaign("", 0, test);
    auto output = getHighestPriceVector(5u, camp_and_country);

    AssertVectorEqual(output, result, "");
}

void Test_empty_campaign()
{
    vector<Banner> test = { Banner(2,0, {FR,RU},1000) , Banner(3, 0, { IT,RU }, 1001), Banner(3, 0, { FR,IT }, 1002) };
    vector<Banner> result = {};

    auto camp_and_country = getByCountryAndCampaign("", 1, test);
    auto output = getHighestPriceVector(5u, camp_and_country);

    AssertVectorEqual(output, result, "");
}

void Test_with_empty_campaign()
{
    vector<Banner> test = { Banner(2,0, {FR,RU},1000) , Banner(3, 0, { IT,RU }, 1001), Banner(3, 0, { "",IT }, 1002) };
    vector<Banner> result = { Banner(2,0, {FR,RU},1000) , Banner(3, 0, { "",IT }, 1002) };

    auto camp_and_country = getByCountryAndCampaign("FR", 0, test);
    auto output = getHighestPriceVector(5u, camp_and_country);

    AssertVectorEqual(output, result, "");
}

void Test_with_only_empty_campaign()
{
    vector<Banner> test = { Banner(2,0, {FR,RU},1000) , Banner(3, 0, { IT,RU }, 1001), Banner(3, 0, { "",IT }, 1002) };
    vector<Banner> result = {  Banner(3, 0, { "",IT }, 1002) };

    auto camp_and_country = getByCountryAndCampaign("FR", 0, test);
    auto output = getHighestPriceVector(1u, camp_and_country);

    AssertVectorEqual(output, result, "");
}



void Test_basic_1()
{
    vector<Banner> test = { 
        Banner(2, 0, {FR,RU}, 1000) , 
        Banner(3, 1, { IT,RU }, 1001), 
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 1, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004)
    };
    vector<Banner> result = { Banner(3, 1, { IT,RU }, 1001), Banner(1, 1, { DE,IT,RU }, 1003) };

    auto camp_and_country = getByCountryAndCampaign("IT", 1, test);
    auto output = getHighestPriceVector(5u, camp_and_country);

    AssertVectorEqual(output, result, "");
}

void Test_basic_2()
{
    vector<Banner> test = {
        Banner(2, 0, {FR,RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 1, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(2, 0, { DE,RU,FR }, 1005)
    };
    vector<Banner> result = { Banner(2, 0, {FR,RU}, 1000), Banner(2, 0, { DE,RU,FR }, 1005) };

    auto camp_and_country = getByCountryAndCampaign("RU", 0, test);
    auto output = getHighestPriceVector(5u, camp_and_country);

    AssertVectorEqual(output, result, "");
}

void Test_basic_3()
{
    vector<Banner> test = {
        Banner(2, 0, {FR,RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 1, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(2, 0, { DE,RU,FR }, 1005)
    };
    vector<Banner> result = { Banner(3, 1, { IT,RU }, 1001), Banner(1, 1, { DE,IT,RU }, 1003), Banner(2, 1, { DE,RU,FR }, 1004), };

    auto camp_and_country = getByCountryAndCampaign("RU", 1, test);
    auto output = getHighestPriceVector(5u, camp_and_country);

    AssertVectorEqual(output, result, "");
}

void Test_basic_4()
{
    vector<Banner> test = {
        Banner(2, 0, {FR,RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 1, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(2, 0, { DE,RU,FR }, 1005)
    };
    vector<Banner> result = { Banner(3, 1, { IT,RU }, 1001), Banner(2, 1, { DE,RU,FR }, 1004) };

    auto camp_and_country = getByCountryAndCampaign("RU", 1, test);
    auto output = getHighestPriceVector(2u, camp_and_country);

    AssertVectorEqual(output, result, "");
}

void Test_basic_5()
{
    vector<Banner> test = {
        Banner(2, 0, {FR,RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 1, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(2, 0, { DE,RU,FR }, 1005)
    };
    vector<Banner> result = { Banner(3, 1, { IT,RU }, 1001), Banner(1, 1, { DE,IT,RU }, 1003), Banner(2, 1, { DE,RU,FR }, 1004) };

    auto camp_and_country = getByCountryAndCampaign("RU", 1, test);
    auto output = getHighestPriceVector(5u, camp_and_country);

    AssertVectorEqual(output, result, "");
}

void Test_basic_6()
{
    vector<Banner> test = {
        Banner(2, 0, {FR,RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 1, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(2, 0, { DE,RU,FR }, 1005)
    };
    vector<Banner> result = { Banner(2, 1, { DE,RU,FR }, 1004) };

    auto camp_and_country = getByCountryAndCampaign("DE", 1, test);
    auto output = getHighestPriceVector(1u, camp_and_country);

    AssertVectorEqual(output, result, "");
}

void Test_value_1()
{
    vector<Banner> test = {
        Banner(2, 0, {FR,RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 1, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(2, 0, { DE,RU,FR }, 1005)
    };
    vector<Banner> output = getHighest(9u, FR, test);
    int value = 0;
    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 7, "");
}

void Test_value_2()
{
    vector<Banner> test = {
        Banner(2, 0, {FR,RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 1, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(1, 0, { DE,RU,FR }, 1005)
    };
    vector<Banner> output = getHighest(2u, FR, test);
    int value = 0;
    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 5, "");
}


void Test_value_3()
{
    vector<Banner> test = {
        Banner(2, 0, {FR,RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 1, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(1, 0, { DE,RU,FR }, 1005)
    };
    vector<Banner> output = getHighest(0u, FR, test);
    int value = 0;
    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 0, "");
}


void Test_value_4()
{
    vector<Banner> test = {
        Banner(2, 0, {FR,RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 2, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(1, 0, { DE,IT,RU }, 1005),
        Banner(1, 2, { DE,RU,FR }, 1006),
        Banner(3, 2, { DE,RU }, 1007)
    };
    vector<Banner> output = getHighest(10u, DE, test);
    int value = 0;
    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 5, "");
}

void Test_value_5()
{
    vector<Banner> test = {
        Banner(2, 0, {"",RU}, 1000) ,
        Banner(1, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 2, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(3, 0, { DE,IT,RU }, 1005),
        Banner(1, 2, { DE,RU,FR }, 1006),
        Banner(3, 2, { DE,RU }, 1007)
    };
    vector<Banner> output = getHighest(10u, RU, test);
    int value = 0;
    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 5, "");
}

void Test_value_6()
{
    vector<Banner> test = {
        Banner(2, 0, {"",RU}, 1000) ,
        Banner(1, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 2, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(3, 0, { DE,IT,RU }, 1005),
        Banner(1, 2, { DE,RU,FR }, 1006),
        Banner(3, 2, { DE,RU }, 1007)
    };
    vector<Banner> output = getHighest(2u, IT, test);
    int value = 0;
    
    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 6, "");
}

void Test_value_7()
{
    vector<Banner> test = {
        Banner(2, 0, {"",RU}, 1000) ,
        Banner(1, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 2, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(3, 0, { DE,IT,RU }, 1005),
        Banner(1, 2, { DE,RU,FR }, 1006),
        Banner(3, 2, { DE,RU }, 1007)
    };
    vector<Banner> output = getHighest(1u, IT, test);
    int value = 0;

    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 3, "");
}

void Test_value_8()
{
    vector<Banner> test = {
        Banner(4, 0, {"",RU}, 1000) ,
        Banner(3, 0, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 2, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(3, 0, { DE,IT,RU }, 1005),
        Banner(1, 2, { DE,RU,FR }, 1006),
        Banner(3, 2, { DE,RU }, 1007)
    };
    vector<Banner> output = getHighest(3u, IT, test);
    int value = 0;

    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 10, "");
}


void Test_value_9()
{
    vector<Banner> test = {
        Banner(4, 0, {"",RU}, 1000) ,
        Banner(3, 0, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 2, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(3, 0, { DE,IT,RU }, 1005),
        Banner(1, 2, { DE,RU,FR }, 1006),
        Banner(3, 2, { DE,RU }, 1007)
    };
    vector<Banner> output = getHighest(2u, IT, test);
    int value = 0;

    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 7, "");
}


void Test_value_10()
{
    vector<Banner> test = {
        Banner(4, 0, {"",RU}, 1000) ,
        Banner(3, 0, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 2, { DE,IT,RU }, 1003),
        Banner(2, 1, { DE,RU,FR }, 1004),
        Banner(3, 0, { DE,IT,RU }, 1005),
        Banner(1, 2, { DE,RU,FR }, 1006),
        Banner(3, 2, { DE,RU }, 1007)
    };
    vector<Banner> output = getHighest(0u, IT, test);
    int value = 0;

    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 0, "");
}


void Test_value_11()
{
    vector<Banner> test = {
        Banner(2, 0, {"",RU}, 1000) ,
        Banner(3, 1, { IT,RU }, 1001),
        Banner(3, 0, { FR,IT }, 1002),
        Banner(1, 2, { DE,IT,RU }, 1003),
        Banner(2, 1, { IT,RU,FR }, 1004),
        Banner(3, 0, { DE,IT,RU }, 1005),
        Banner(1, 2, { DE,RU,FR }, 1006),
        Banner(3, 2, { DE,RU }, 1007)
    };
    vector<Banner> output = getHighest(5u, DE, test);
    int value = 0;

    for (auto& item : output)
    {
        value = value + item.getPrice();
    }

    AssertEqual(value, 5, "");
}


void TestAll() 
{
	TestRunner tr;

	tr.RunTest(Test_basic, "Test_basic");
    tr.RunTest(Test_empty_country, "Test_empty_country");
    tr.RunTest(Test_with_empty_campaign, "Test_with_empty_campaign");
    tr.RunTest(Test_with_only_empty_campaign, "Test_with_only_empty_campaign");

    tr.RunTest(Test_basic_1, "Test_basic_1");
    tr.RunTest(Test_basic_2, "Test_basic_2");
    tr.RunTest(Test_basic_3, "Test_basic_3");
    tr.RunTest(Test_basic_4, "Test_basic_4");
    tr.RunTest(Test_basic_5, "Test_basic_5");
    tr.RunTest(Test_basic_6, "Test_basic_6");

    tr.RunTest(Test_value_1, "Test_value_1");
    tr.RunTest(Test_value_2, "Test_value_2");
    tr.RunTest(Test_value_3, "Test_value_3");
    tr.RunTest(Test_value_4, "Test_value_4");
    tr.RunTest(Test_value_5, "Test_value_5");
    tr.RunTest(Test_value_6, "Test_value_6");
    tr.RunTest(Test_value_7, "Test_value_7");
    tr.RunTest(Test_value_8, "Test_value_8");
    tr.RunTest(Test_value_9, "Test_value_9");
    tr.RunTest(Test_value_10, "Test_value_10");
    tr.RunTest(Test_value_11, "Test_value_11");
//	tr.RunTest(TestCount, "TestCount");
///	tr.RunTest(TestAreSynonyms, "TestAreSynonyms");
}

